package com.pimp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PimpApplication {

	public static void main(String[] args) {
		SpringApplication.run(PimpApplication.class, args);
	}

}
