package com.pimp.controller;

import com.pimp.service.AppointmentService;
import com.pimp.service.dto.AppointmentDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api/appointment")
public class AppointmentController {

    private final Logger logger = LogManager.getLogger(AppointmentController.class);

    private final AppointmentService appointmentService;

    private final String ENTITY_NAME = "Appointment";

    private final String SERVER_MESSAGE = "Server Error";

    public AppointmentController(AppointmentService appointmentService) {
        this.appointmentService = appointmentService;

    }

    @PostMapping("/create")
    public ResponseEntity<AppointmentDTO> createAppointment(@RequestBody AppointmentDTO appointmentDTO) {
        logger.info("REST request to save Appointment : {}", appointmentDTO);
        String message = " ";
        try {
            if (appointmentDTO.getId() != null) {
                message = "A new Appointment cannot already have an ID";
                return null;
            }
            AppointmentDTO result = appointmentService.save(appointmentDTO);
            return ResponseEntity.created(new URI("/api/appointment/" + result.getId()))
                    .body(result);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header("SERVER_MESSAGE").body(null);
        }

    }

    @PutMapping("/update")
    public ResponseEntity<AppointmentDTO> updateUser(@RequestBody AppointmentDTO appointmentDTO) {
        logger.info("REST request to update Appointment : {}", appointmentDTO);
        String message = " ";
        try {
            if (appointmentDTO.getId() == null) {
                message = "The updated appointment have id";
                return null;
            }
            AppointmentDTO result = appointmentService.save(appointmentDTO);

            return ResponseEntity.ok()
                    .body(result);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteAppointment(@PathVariable Long id) {
        logger.info("REST request to delete Appointment : {}", id);
        try {
            appointmentService.delete(id);
            return ResponseEntity.ok().build();
        } catch (EmptyResultDataAccessException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    @GetMapping("/allappointments")
    public ResponseEntity<List<AppointmentDTO>> getAllappointments() {
        try {
            logger.info("REST request to get all Appointments");
            List<AppointmentDTO> result = appointmentService.findAll();
            if (result.size() == 0) {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
            }
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

}



