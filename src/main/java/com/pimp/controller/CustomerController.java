package com.pimp.controller;


import com.pimp.service.CustomerService;
import com.pimp.service.dto.CustomerDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api/customer")
public class CustomerController {

    private final Logger logger = LogManager.getLogger(CustomerController.class);

    private final CustomerService customerService;

    private final String ENTITY_NAME = "Customer";

    private final String SERVER_MESSAGE = "Server Error";

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;

    }

    @PostMapping("/create")
    public ResponseEntity<CustomerDTO> createCustomer(@RequestBody CustomerDTO customerDTO) {
        logger.info("REST request to save Customer : {}", customerDTO);
        String message = " ";
        try {
            if (customerDTO.getId() != null) {
                message = "A new Customer cannot already have an ID";
                return null;
            }
            CustomerDTO result = customerService.save(customerDTO);
            return ResponseEntity.created(new URI("/api/customer/" + result.getId()))
                    .body(result);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header("SERVER_MESSAGE").body(null);
        }

    }

    @PutMapping("/update")
    public ResponseEntity<CustomerDTO> updateUser(@RequestBody CustomerDTO customerDTO) {
        logger.info("REST request to update Customer : {}", customerDTO);
        String message = " ";
        try {
            if (customerDTO.getId() == null) {
                message = "The updated Customer have id";
                return null;
            }
            CustomerDTO result = customerService.save(customerDTO);

            return ResponseEntity.ok()
                    .body(result);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCustomer(@PathVariable Long id) {
        logger.info("REST request to delete Customer : {}", id);
        try {
            customerService.delete(id);
            return ResponseEntity.ok().build();
        } catch (EmptyResultDataAccessException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    @GetMapping("/allCustomers")
    public ResponseEntity<List<CustomerDTO>> getAllCustomers() {
        try {
            logger.info("REST request to get all Customers");
            List<CustomerDTO> result = customerService.findAll();
            if (result.size() == 0) {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
            }
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

}




