package com.pimp.controller;

import com.pimp.service.EmployeeService;
import com.pimp.service.dto.EmployeeDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api/employee")
public class EmployeeController {

    private final Logger logger = LogManager.getLogger(EmployeeController.class);

    private final EmployeeService employeeService;

    private final String ENTITY_NAME = "Employee";

    private final String SERVER_MESSAGE = "Server Error";

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;

    }

    @PostMapping("/create")
    public ResponseEntity<EmployeeDTO> createEmployee(@RequestBody EmployeeDTO employeeDTO) {
        logger.info("REST request to save Employee : {}", employeeDTO);
        String message = " ";
        try {
            if (employeeDTO.getId() != null) {
                message = "A new Employee cannot already have an ID";
                return null;
            }
            EmployeeDTO result = employeeService.save(employeeDTO);
            return ResponseEntity.created(new URI("/api/employee/" + result.getId()))
                    .body(result);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header("SERVER_MESSAGE").body(null);
        }

    }

    @PutMapping("/update")
    public ResponseEntity<EmployeeDTO> updateUser(@RequestBody EmployeeDTO employeeDTO) {
        logger.info("REST request to update Employee : {}", employeeDTO);
        String message = " ";
        try {
            if (employeeDTO.getId() == null) {
                message = "The updated Employee have id";
                return null;
            }
            EmployeeDTO result = employeeService.save(employeeDTO);

            return ResponseEntity.ok()
                    .body(result);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteEmployee(@PathVariable Long id) {
        logger.info("REST request to delete Employee : {}", id);
        try {
            employeeService.delete(id);
            return ResponseEntity.ok().build();
        } catch (EmptyResultDataAccessException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    @GetMapping("/allEmployees")
    public ResponseEntity<List<EmployeeDTO>> getAllEmployees() {
        try {
            logger.info("REST request to get all Employees");
            List<EmployeeDTO> result = employeeService.findAll();
            if (result.size() == 0) {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
            }
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }
}
