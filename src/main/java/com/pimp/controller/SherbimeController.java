package com.pimp.controller;

import com.pimp.service.SherbimeService;
import com.pimp.service.dto.SherbimeDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.List;


@RestController
@RequestMapping("/api/sherbime")
public class SherbimeController {

    private final Logger logger = LogManager.getLogger(SherbimeController.class);

    private final SherbimeService sherbimeService;

    private final String ENTITY_NAME = "Sherbime";

    private final String SERVER_MESSAGE = "Server Error";

    public SherbimeController(SherbimeService sherbimeService) {
        this.sherbimeService = sherbimeService;

    }

    @PostMapping("/create")
    public ResponseEntity<SherbimeDTO> createSherbime(@RequestBody SherbimeDTO sherbimeDTO) {
        logger.info("REST request to save Sherbime : {}", sherbimeDTO);
        String message = " ";
        try {
            if (sherbimeDTO.getId() != null) {
                message = "A new Sherbime cannot already have an ID";
                return null;
            }
            SherbimeDTO result = sherbimeService.save(sherbimeDTO);
            return ResponseEntity.created(new URI("/api/sherbime/" + result.getId()))
                    .body(result);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header("SERVER_MESSAGE").body(null);
        }

    }

    @PutMapping("/update")
    public ResponseEntity<SherbimeDTO> updateUser(@RequestBody SherbimeDTO sherbimeDTO) {
        logger.info("REST request to update Sherbime : {}", sherbimeDTO);
        String message = " ";
        try {
            if (sherbimeDTO.getId() == null) {
                message = "The updated Sherbime have id";
                return null;
            }
            SherbimeDTO result = sherbimeService.save(sherbimeDTO);

            return ResponseEntity.ok()
                    .body(result);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteSherbime(@PathVariable Long id) {
        logger.info("REST request to delete Sherbime : {}", id);
        try {
            sherbimeService.delete(id);
            return ResponseEntity.ok().build();
        } catch (EmptyResultDataAccessException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    @GetMapping("/allSherbimes")
    public ResponseEntity<List<SherbimeDTO>> getAllSherbimes() {
        try {
            logger.info("REST request to get all Sherbimes");
            List<SherbimeDTO> result = sherbimeService.findAll();
            if (result.size() == 0) {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
            }
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }
}
