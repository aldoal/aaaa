package com.pimp.controller;

import com.pimp.service.VenueService;
import com.pimp.service.dto.VenueDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api/venue")
public class VenueController {
    private final Logger logger = LogManager.getLogger(VenueController.class);

    private final VenueService venueService;

    private final String ENTITY_NAME = "Venue";

    private final String SERVER_MESSAGE = "Server Error";

    public VenueController(VenueService venueService) {
        this.venueService = venueService;

    }

    @PostMapping("/create")
    public ResponseEntity<VenueDTO> createVenue(@RequestBody VenueDTO venueDTO) {
        logger.info("REST request to save Venue : {}", venueDTO);
        String message = " ";
        try {
            if (venueDTO.getId() != null) {
                message = "A new Venue cannot already have an ID";
                return null;
            }
            VenueDTO result = venueService.save(venueDTO);
            return ResponseEntity.created(new URI("/api/venue/" + result.getId()))
                    .body(result);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header("SERVER_MESSAGE").body(null);
        }

    }

    @PutMapping("/update")
    public ResponseEntity<VenueDTO> updateUser(@RequestBody VenueDTO venueDTO) {
        logger.info("REST request to update Venue : {}", venueDTO);
        String message = " ";
        try {
            if (venueDTO.getId() == null) {
                message = "The updated Venue have id";
                return null;
            }
            VenueDTO result = venueService.save(venueDTO);

            return ResponseEntity.ok()
                    .body(result);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteVenue(@PathVariable Long id) {
        logger.info("REST request to delete Venue : {}", id);
        try {
            venueService.delete(id);
            return ResponseEntity.ok().build();
        } catch (EmptyResultDataAccessException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    @GetMapping("/allVenues")
    public ResponseEntity<List<VenueDTO>> getAllVenues() {
        try {
            logger.info("REST request to get all Venues");
            List<VenueDTO> result = venueService.findAll();
            if (result.size() == 0) {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
            }
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }
}
