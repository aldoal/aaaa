package com.pimp.controller.config;


import com.pimp.service.dto.UserDTO;

public class AuthToken {

    private String token;
    private UserDTO user;

    public AuthToken(){

    }

    public AuthToken(String token, UserDTO user){
        this.token = token;
        this.user = user;
    }

    public AuthToken(String token){
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }
}