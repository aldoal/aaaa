package com.pimp.model;

import com.pimp.model.baseconfiguration.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "appointment")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Appointment extends BaseEntity {

    private LocalDateTime appointmentDateTime;

    @ManyToOne
    private Customer customer;

    @ManyToMany
    @JoinTable(name = "appointment_sherbime",
            joinColumns = @JoinColumn(name = "appointment_id"),
            inverseJoinColumns = @JoinColumn(name = "sherbime_id"))
    private Set<Sherbime> appointmentSherbime = new HashSet<>();

}
