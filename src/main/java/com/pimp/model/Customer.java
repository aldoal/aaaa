package com.pimp.model;


import com.pimp.model.baseconfiguration.BaseEntity;
import com.pimp.model.enumeration.Gender;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name = "customer")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Customer extends BaseEntity {

    private String area;

    private String name;

    private String city;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    private Long phoneNumber;

    @OneToMany(mappedBy = "customer")
    private Set<Appointment> appointments = new HashSet<>();

    @OneToOne(mappedBy = "customer")
    private User user;


}
