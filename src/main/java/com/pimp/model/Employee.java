package com.pimp.model;

import com.pimp.model.baseconfiguration.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "employee")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Employee extends BaseEntity {

    private String name;

    private LocalDateTime closeHour;

    private LocalDateTime openHour;

    private Long phoneNumber;

    @ManyToOne
    private Venue venue;

    @ManyToMany
    @JoinTable(name = "employee_sherbime",
            joinColumns = @JoinColumn(name = "employee_id"),
            inverseJoinColumns = @JoinColumn(name = "sherbime_id"))
    private Set<Sherbime>  employeeSherbime = new HashSet<>();

    @OneToOne(mappedBy = "employee")
    private User user;
}
