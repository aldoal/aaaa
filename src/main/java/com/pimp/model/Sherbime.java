package com.pimp.model;


import com.pimp.model.baseconfiguration.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.time.Duration;
import java.util.Set;

@Entity
@Table(name = "service")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Sherbime extends BaseEntity {

    private String name;

    private Long price;

    private Duration serviceDuration;

    @ManyToMany(mappedBy = "appointmentSherbime")
    Set<Appointment> appointmentSherbime;

    @ManyToMany(mappedBy = "employeeSherbime")
    Set<Employee> employeeSherbime;

    @ManyToMany(mappedBy = "venueSherbime")
    Set<Venue> venueSherbime;

}
