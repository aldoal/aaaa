package com.pimp.model;


import com.pimp.model.baseconfiguration.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "venue")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Venue extends BaseEntity {

    private String area;

    private String city;

    private LocalDateTime closeHour;

    private String name;

    private String nipt;

    private Long phoneNumber;

    private LocalDateTime openHour;

    @OneToMany(mappedBy = "venue")
    private Set<Employee> employees = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "venue_sherbime",
            joinColumns = @JoinColumn(name = "venue_id"),
            inverseJoinColumns = @JoinColumn(name = "sherbime_id"))
    private Set<Sherbime> venueSherbime = new HashSet<>();

    @OneToOne(mappedBy = "venue")
    private User user;

}
