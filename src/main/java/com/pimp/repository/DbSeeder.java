package com.pimp.repository;

import com.pimp.model.User;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;


@Component
public class DbSeeder implements CommandLineRunner {


    private UserRepository userRepository;

    public DbSeeder(UserRepository userRepository){
        this.userRepository = userRepository;
    }


    //save user admin
    @Override
    public void run(String... args) throws Exception {
        if (!userRepository.existsByUsername("admin")) {
            User user = new User("admin", "admin");
            userRepository.save(user);
        }
    }
}
