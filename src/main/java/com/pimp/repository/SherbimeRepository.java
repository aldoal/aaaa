package com.pimp.repository;

import com.pimp.model.Sherbime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SherbimeRepository extends JpaRepository<Sherbime, Long> {
}
