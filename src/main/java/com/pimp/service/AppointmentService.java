package com.pimp.service;


import com.pimp.service.dto.AppointmentDTO;

import java.util.List;
import java.util.Optional;

public interface AppointmentService {

    AppointmentDTO save(AppointmentDTO appointmentDTO);

    List<AppointmentDTO> findAll();

    Optional<AppointmentDTO> findOne(Long id);

    void delete(Long id);

}
