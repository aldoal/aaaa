package com.pimp.service;

import com.pimp.service.dto.CustomerDTO;

import java.util.List;
import java.util.Optional;

public interface CustomerService {

    CustomerDTO save(CustomerDTO customerDTO);

    List<CustomerDTO> findAll();

    Optional<CustomerDTO> findOne(Long id);

    void delete(Long id);
}
