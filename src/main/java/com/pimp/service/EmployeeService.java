package com.pimp.service;

import com.pimp.service.dto.EmployeeDTO;

import java.util.List;
import java.util.Optional;

public interface EmployeeService {

    EmployeeDTO save(EmployeeDTO employeeDTO);

    List<EmployeeDTO> findAll();

    Optional<EmployeeDTO> findOne(Long id);

    void delete(Long id);
    
}
