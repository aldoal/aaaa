package com.pimp.service;


import com.pimp.service.dto.SherbimeDTO;

import java.util.List;
import java.util.Optional;

public interface SherbimeService {

    SherbimeDTO save(SherbimeDTO sherbimeDTO);

    List<SherbimeDTO> findAll();

    Optional<SherbimeDTO> findOne(Long id);

    void delete(Long id);
}
