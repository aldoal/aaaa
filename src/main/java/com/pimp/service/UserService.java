package com.pimp.service;

import com.pimp.service.dto.UserDTO;

import java.util.Optional;

public interface UserService {

    Optional<UserDTO> findbyUsername(String username);
}
