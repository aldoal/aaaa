package com.pimp.service;

import com.pimp.service.dto.VenueDTO;

import java.util.List;
import java.util.Optional;

public interface VenueService {

    VenueDTO save(VenueDTO VenueDTO);

    List<VenueDTO> findAll();

    Optional<VenueDTO> findOne(Long id);

    void delete(Long id);

}
