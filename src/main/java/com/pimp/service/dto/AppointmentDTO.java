package com.pimp.service.dto;

import com.pimp.model.baseconfiguration.BaseEntity;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Data
@NoArgsConstructor
@ToString
public class AppointmentDTO extends BaseEntity implements Serializable {

    private LocalDateTime appointmentDateTime;

    private Set<SherbimeDTO> appointmentSherbime = new HashSet<>();

    private CustomerDTO customer;
}
