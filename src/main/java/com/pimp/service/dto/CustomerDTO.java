package com.pimp.service.dto;


import com.pimp.model.User;
import com.pimp.model.baseconfiguration.BaseEntity;
import com.pimp.model.enumeration.Gender;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
@ToString
public class CustomerDTO extends BaseEntity implements Serializable {

    private String name;

    private String area;

    private Long phoneNumber;

    private String city;

    private Gender gender;

    private Set<AppointmentDTO> appointments = new HashSet<>();

    private UserDTO user;
}
