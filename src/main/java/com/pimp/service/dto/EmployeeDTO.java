package com.pimp.service.dto;

import com.pimp.model.baseconfiguration.BaseEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class EmployeeDTO extends BaseEntity implements Serializable {

    private String name;

    private LocalDateTime openHour;

    private LocalDateTime closeHour;

    private VenueDTO venue;

    private Long phoneNumber;

    private Set<SherbimeDTO> employeeSherbime = new HashSet<>();

    private EmployeeDTO employee;
}
