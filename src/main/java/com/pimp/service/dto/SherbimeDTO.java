package com.pimp.service.dto;

import com.pimp.model.baseconfiguration.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.time.Duration;
import java.util.HashSet;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
@ToString
public class SherbimeDTO extends BaseEntity implements Serializable {

    private String name;

    private Long price;

    private Duration serviceDuration;

    Set<VenueDTO> venueSherbime = new HashSet<>();

    Set<EmployeeDTO> employeeSherbime  = new HashSet<>();

    Set<AppointmentDTO> appointmentSherbime = new HashSet<>();

}
