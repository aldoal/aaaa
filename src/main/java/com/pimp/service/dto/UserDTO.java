package com.pimp.service.dto;

import com.pimp.model.baseconfiguration.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class UserDTO extends BaseEntity implements Serializable {

    private String password;

    private String username;

    private CustomerDTO customer;

    private EmployeeDTO employee;

    private VenueDTO venue;


}
