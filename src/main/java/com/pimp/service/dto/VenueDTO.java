package com.pimp.service.dto;

import com.pimp.model.Venue;
import com.pimp.model.baseconfiguration.BaseEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
@ToString
public class VenueDTO extends BaseEntity implements Serializable {

    private String name;

    private String nipt;

    private String city;

    private String area;

    private LocalDateTime openHour;

    private LocalDateTime closeHour;

    private Long phoneNumber;

    private Set<EmployeeDTO> employees = new HashSet<>();

    private Set<SherbimeDTO> venueSherbime = new HashSet<>();

    private VenueDTO venue;

    private UserDTO user;

}
