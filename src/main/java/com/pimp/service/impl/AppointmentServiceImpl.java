package com.pimp.service.impl;

import com.pimp.model.Appointment;
import com.pimp.repository.AppointmentRepository;
import com.pimp.service.AppointmentService;
import com.pimp.service.dto.AppointmentDTO;
import com.pimp.service.mapper.AppointmentMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.LinkedList;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AppointmentServiceImpl implements AppointmentService {

    private final Logger logger =  LogManager.getLogger(AppointmentServiceImpl.class);

    private final AppointmentRepository appointmentRepository;

    private final AppointmentMapper appointmentMapper;

    public AppointmentServiceImpl(AppointmentRepository appointmentRepository, AppointmentMapper appointmentMapper) {
        this.appointmentRepository = appointmentRepository;
        this.appointmentMapper = appointmentMapper;
    }

    @Override
    public AppointmentDTO save(AppointmentDTO appointmentDTO) {
        logger.info("Request to save Appointment : {}", appointmentDTO);
        Appointment appointment = appointmentMapper.toEntity(appointmentDTO);
        appointment = appointmentRepository.save(appointment);
        return appointmentMapper.toDto(appointment);
    }

    @Override
    public List<AppointmentDTO> findAll() {
        logger.info("Request to find All Appointment : {}");
        return appointmentRepository.findAll().stream().map(appointmentMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public Optional<AppointmentDTO> findOne(Long id) {
        logger.info("Request to get id : {}", id);
        return appointmentRepository.findById(id)
                .map(appointmentMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        logger.info("Request to delete appointment : {}", id);
        appointmentRepository.deleteById(id);
    }
}
