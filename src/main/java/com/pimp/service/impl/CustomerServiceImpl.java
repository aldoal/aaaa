package com.pimp.service.impl;


import com.pimp.model.Customer;
import com.pimp.repository.CustomerRepository;
import com.pimp.service.CustomerService;
import com.pimp.service.dto.CustomerDTO;
import com.pimp.service.mapper.CustomerMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.LinkedList;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CustomerServiceImpl implements CustomerService {
    private final Logger logger = LogManager.getLogger(CustomerServiceImpl.class);

    private final CustomerRepository customerRepository;

    private final CustomerMapper customerMapper;

    public CustomerServiceImpl(CustomerRepository customerRepository, CustomerMapper customerMapper) {
        this.customerRepository = customerRepository;
        this.customerMapper = customerMapper;
    }

    @Override
    public CustomerDTO save(CustomerDTO CustomerDTO) {
        logger.info("Request to save Customer : {}", CustomerDTO);
        Customer customer = customerMapper.toEntity(CustomerDTO);
        customer = customerRepository.save(customer);
        return customerMapper.toDto(customer);
    }

    @Override
    public List<CustomerDTO> findAll() {
        logger.info("Request to find All : {}");
        return customerRepository.findAll().stream().map(customerMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public Optional<CustomerDTO> findOne(Long id) {
        logger.info("Request to get id : {}", id);
        return customerRepository.findById(id).map(customerMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        logger.info("Request to delete Customer : {}", id);
        customerRepository.deleteById(id);
    }
}



