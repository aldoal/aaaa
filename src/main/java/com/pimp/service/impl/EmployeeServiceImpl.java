package com.pimp.service.impl;


import com.pimp.model.Employee;
import com.pimp.repository.EmployeeRepository;
import com.pimp.service.EmployeeService;
import com.pimp.service.dto.EmployeeDTO;
import com.pimp.service.mapper.EmployeeMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    private final Logger logger = LogManager.getLogger(EmployeeServiceImpl.class);

    private final EmployeeRepository employeeRepository;

    private final EmployeeMapper employeeMapper;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository, EmployeeMapper employeeMapper) {
        this.employeeRepository = employeeRepository;
        this.employeeMapper = employeeMapper;
    }

    @Override
    public EmployeeDTO save(EmployeeDTO employeeDTO) {
        logger.info("Request to save Employee : {}", employeeDTO);
        Employee employee = employeeMapper.toEntity(employeeDTO);
        employee = employeeRepository.save(employee);
        return employeeMapper.toDto(employee);
    }



    @Override
    public List<EmployeeDTO> findAll() {
        logger.info("Request to find All : {}");
        return employeeRepository.findAll().stream().map(employeeMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public Optional<EmployeeDTO> findOne(Long id) {
        logger.info("Request to get id : {}", id);
        return employeeRepository.findById(id)
                .map(employeeMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        logger.info("Request to delete Employee : {}", id);
        employeeRepository.deleteById(id);
    }
}
