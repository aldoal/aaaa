package com.pimp.service.impl;


import com.pimp.model.Sherbime;
import com.pimp.repository.SherbimeRepository;
import com.pimp.service.SherbimeService;
import com.pimp.service.dto.SherbimeDTO;
import com.pimp.service.mapper.SherbimeMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SherbimeServiceImpl implements SherbimeService {
    
    private final Logger logger = LogManager.getLogger(SherbimeServiceImpl.class);

    private final SherbimeRepository sherbimeRepository;

    private final SherbimeMapper sherbimeMapper;

    public SherbimeServiceImpl(SherbimeRepository sherbimeRepository, SherbimeMapper sherbimeMapper) {
        this.sherbimeRepository = sherbimeRepository;
        this.sherbimeMapper = sherbimeMapper;
    }

    @Override
    public SherbimeDTO save(SherbimeDTO sherbimeDTO) {
        logger.info("Request to save Sherbime : {}", sherbimeDTO);
        Sherbime sherbime = sherbimeMapper.toEntity(sherbimeDTO);
        sherbime = sherbimeRepository.save(sherbime);
        return sherbimeMapper.toDto(sherbime);
    }



    @Override
    public List<SherbimeDTO> findAll() {
        logger.info("Request to find All Sherbime : {}");
        return sherbimeRepository.findAll().stream().map(sherbimeMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public Optional<SherbimeDTO> findOne(Long id) {
        logger.info("Request to get id : {}", id);
        return sherbimeRepository.findById(id).map(sherbimeMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        logger.info("Request to delete Sherbime : {}", id);
        sherbimeRepository.deleteById(id);
    }
}
