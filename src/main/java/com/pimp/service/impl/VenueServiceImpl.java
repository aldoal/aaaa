package com.pimp.service.impl;


import com.pimp.model.Venue;
import com.pimp.repository.VenueRepository;
import com.pimp.service.VenueService;
import com.pimp.service.dto.VenueDTO;
import com.pimp.service.mapper.VenueMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.LinkedList;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class VenueServiceImpl implements VenueService {
    
    private final Logger logger = LogManager.getLogger(VenueServiceImpl.class);

    private final VenueRepository venueRepository;

    private final VenueMapper venueMapper;

    public VenueServiceImpl(VenueRepository venueRepository, VenueMapper venueMapper) {
        this.venueRepository = venueRepository;
        this.venueMapper = venueMapper;
    }

    @Override
    public VenueDTO save(VenueDTO venueDTO) {
        logger.info("Request to save Venue : {}", venueDTO);
        Venue venue = venueMapper.toEntity(venueDTO);
        venue = venueRepository.save(venue);
        return venueMapper.toDto(venue);
    }



    @Override
    public List<VenueDTO> findAll() {
        logger.info("Request to find All Venue : {}");
        return venueRepository.findAll().stream().map(venueMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public Optional<VenueDTO> findOne(Long id) {
        logger.info("Request to get id : {}", id);
        return venueRepository.findById(id).map(venueMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        logger.info("Request to delete Venue : {}", id);
        venueRepository.deleteById(id);
    }
}