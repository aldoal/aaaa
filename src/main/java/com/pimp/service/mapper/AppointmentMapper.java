package com.pimp.service.mapper;



import com.pimp.model.Appointment;
import com.pimp.service.dto.AppointmentDTO;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {SherbimeMapper.class, CustomerMapper.class})
public interface AppointmentMapper extends EntityMapper<AppointmentDTO, Appointment> {

    @Mapping(source = "appointmentSherbime", target = "appointmentSherbime")
    @Mapping(source = "customer", target = "customer")
    AppointmentDTO toDto(Appointment appointment);

    @Mapping(source = "appointmentSherbime", target = "appointmentSherbime")
    @Mapping(source = "customer", target = "customer")
    Appointment toEntity(AppointmentDTO appointmentDTO);

}
