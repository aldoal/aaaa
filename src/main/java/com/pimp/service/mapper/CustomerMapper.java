package com.pimp.service.mapper;


import com.pimp.model.Customer;
import com.pimp.service.dto.CustomerDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {AppointmentMapper.class,UserMapper.class})
public interface CustomerMapper extends EntityMapper<CustomerDTO, Customer> {

    @Mapping(source = "appointments", target = "appointments")
    CustomerDTO toDto(Customer customer);

    @Mapping(source = "appointments", target = "appointments")
    Customer toEntity(CustomerDTO customerDTO);
}
