package com.pimp.service.mapper;


import com.pimp.model.Employee;
import com.pimp.service.dto.EmployeeDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {VenueMapper.class, SherbimeMapper.class, UserMapper.class})
public interface EmployeeMapper extends EntityMapper<EmployeeDTO, Employee> {

    @Mapping(source = "venue", target = "venue")
    @Mapping(source = "employeeSherbime", target = "employeeSherbime")
    EmployeeDTO toDto(Employee employee);

    @Mapping(source = "venue", target = "venue")
    @Mapping(source = "employeeSherbime", target = "employeeSherbime")
    Employee toEntity(EmployeeDTO employeeDTO);
}