package com.pimp.service.mapper;


import com.pimp.model.Sherbime;
import com.pimp.service.dto.SherbimeDTO;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {AppointmentMapper.class, VenueMapper.class, EmployeeMapper.class})
public interface SherbimeMapper extends EntityMapper<SherbimeDTO, Sherbime> {

    @Mapping(source = "venueSherbime", target = "venueSherbime")
    @Mapping(source = "employeeSherbime", target = "employeeSherbime")
    @Mapping(source = "appointmentSherbime", target = "appointmentSherbime")
    SherbimeDTO toDto(Sherbime sherbime);

    @Mapping(source = "venueSherbime", target = "venueSherbime")
    @Mapping(source = "employeeSherbime", target = "employeeSherbime")
    @Mapping(source = "appointmentSherbime", target = "appointmentSherbime")
    Sherbime toEntity(SherbimeDTO sherbimeDTO);
}
