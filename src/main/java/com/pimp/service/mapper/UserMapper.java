package com.pimp.service.mapper;

import com.pimp.model.User;
import com.pimp.service.dto.UserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {CustomerMapper.class,  EmployeeMapper.class, VenueMapper.class,})
public interface UserMapper extends EntityMapper<UserDTO, User> {

    @Mapping(source = "customer", target = "customer")
    @Mapping(source = "employee", target = "employee")
    @Mapping(source = "venue", target = "venue")
    UserDTO toDto(User User);

    @Mapping(source = "customer", target = "customer")
    @Mapping(source = "employee", target = "employee")
    @Mapping(source = "venue", target = "venue")
    User toEntity(UserDTO userDTO);
}
    

