package com.pimp.service.mapper;

import com.pimp.model.User;
import com.pimp.model.Venue;
import com.pimp.service.dto.VenueDTO;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {EmployeeMapper.class, SherbimeMapper.class, UserMapper.class})
public interface VenueMapper extends EntityMapper<VenueDTO, Venue> {

    @Mapping(source = "employees", target = "employees")
    @Mapping(source = "venueSherbime", target = "venueSherbime")
    @Mapping(source = "user", target = "user")
    VenueDTO toDto(Venue venue);

    @Mapping(source = "employees", target = "employees")
    @Mapping(source = "venueSherbime", target = "venueSherbime")
    @Mapping(source = "user", target = "user")
    Venue toEntity(VenueDTO venueDTO);
}
